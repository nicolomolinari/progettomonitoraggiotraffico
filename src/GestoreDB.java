public class GestoreDB {
    private static GestoreDB ourInstance = new GestoreDB();

    public static GestoreDB getInstance() {
        return ourInstance;
    }

    private GestoreDB() {
    }
}
