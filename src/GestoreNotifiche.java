public class GestoreNotifiche {
    private static GestoreNotifiche ourInstance = new GestoreNotifiche();

    public static GestoreNotifiche getInstance() {
        return ourInstance;
    }

    private GestoreNotifiche() {
    }
}
